package ru.t1.artamonov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.dto.request.ServerAboutRequest;
import ru.t1.artamonov.tm.dto.request.ServerVersionRequest;
import ru.t1.artamonov.tm.dto.response.ServerAboutResponse;
import ru.t1.artamonov.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
